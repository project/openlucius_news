<?php
/**
 * @file
 * This file contains functions for the news.
 */

// Define openlucius status updates limit.
define('OPENLUCIUS_NEWS_INTRO_TEXT_WORDS', 15);
define('OPENLUCIUS_PROMOTED_NEWS_LIMIT', 1);
define('OPENLUCIUS_NEWS_READ_MORE_LIMIT', 140);

/**
 * Function to start a minimal query.
 */
function openlucius_news_basic_query() {

  // Basic select query from node table.
  $query = db_select('node', 'n');

  // Join on the data body table.
  $query->leftJoin('field_data_body', 'b', 'b.entity_id = n.nid');

  // Fetch fields from the heartbeat activity table.
  // Fetch fields from the node table.
  // Only fetch published items.
  // Filter on node type ol news.
  // Respect grants.
  $query->fields('n', array(
    'uid',
    'nid',
    'created',
    'changed',
    'status',
  ))
    ->fields('n', array('title', 'type'))
    ->condition('n.status', NODE_PUBLISHED, '=')
    ->condition('n.type', 'ol_news', '=')
    ->addTag('node_access');

  // Allow other modules to changes the query.
  drupal_alter('openlucius_news_basic_query', $query);

  // Return.
  return $query;
}

/**
 * Get the latest news items and order by heartbeat activity id.
 */
function openlucius_news_get_latest_news_items() {

  // Unique pager id for ajax.
  $pager_id = openlucius_core_pager_counter('news');

  // Get the basic query to build on.
  $query = openlucius_news_basic_query();

  // Fields from the data body table.
  $query->fields('b', array('body_value'));

  // Select not the first promoted item, but wel the others.
  $latest = openlucius_news_get_latest_promoted_news_item();

  // Condition to show all items but that last promoted item.
  $query->condition('n.nid', $latest, '<>');

  // Group items by node id.
  $query->groupBy('n.nid');

  // Extend for the page.
  $query = $query->extend('PagerDefault');

  // Add the element.
  $query->element($pager_id);

  // Get the news limit.
  $limit = variable_get('openlucius_news_news_limit', 20);

  // Limit the output.
  $query->limit($limit);

  // Order by node id descending.
  $query->orderby('nid', 'desc');

  // Allow other modules to changes the query.
  drupal_alter('openlucius_news_get_latest_news_items', $query);

  // Return.
  return $query->execute()->fetchAll();
}

/**
 * Get the latest news items and order by promoted.
 */
function openlucius_news_get_promoted_news_items() {

  // Get the basic query to build on.
  $query = openlucius_news_basic_query();

  // Add a join to the shared files.
  $query->addJoin('left', 'field_data_field_shared_files', 'f', 'f.entity_id = n.nid');

  // Add join to check the file type.
  $query->addJoin('left', 'file_managed', 'm', 'f.entity_id = m.fid');

  // Get the file id.
  $query->fields('f', array('field_shared_files_fid'));

  // Fields from the data body table.
  $query->fields('b', array('body_value'));

  // Order by heartbeat id descending.
  $query->condition('n.promote', NODE_PROMOTED, '=');

  // Order by nid descending.
  $query->orderby('nid', 'desc');

  // Order by promoted descending.
  $query->orderby('n.promote', 'desc');

  // Limit to OPENLUCIUS_PROMOTED_NEWS_LIMIT.
  $query->range(0, OPENLUCIUS_PROMOTED_NEWS_LIMIT);

  // Allow other modules to changes the query.
  drupal_alter('openlucius_news_get_promoted_news_items', $query);

  // Return.
  return $query->execute()->fetchAll();
}

/**
 * Get the latest news items and order by promoted.
 */
function openlucius_news_get_latest_promoted_news_item() {

  // Select the last promoted news item.
  $query = db_select('node', 'n');

  // Select just the node id.
  $query->fields('n', array('nid'));

  // Make sure it is promoted.
  $query->condition('promote', NODE_PROMOTED, '=');

  // Check if it is published.
  $query->condition('status', NODE_PUBLISHED, '=');

  // Order by changed timestamp descending.
  $query->orderby('changed', 'desc');

  // Limit one.
  $query->range(0, 1);

  // Return the field.
  return $query->execute()->fetchField();
}

/**
 * Function to generate the output for the regular news items timeline.
 *
 * @return string
 *   Returns a string of output.
 */
function openlucius_news_build_news_items_display() {

  global $user;

  // Check if user is client and clients may view news items.
  if (variable_get('openlucius_news_show_news_clients', 1) != 1 && openlucius_core_user_is_client()) {
    return "";
  }

  // Load the forms for the reply form.
  module_load_include('openlucius_news', 'inc', 'forms');

  // Initialize the output.
  $output = '';

  // // Add news item link if the user has access.
  if (user_access('create ol_news content')) {
    $output .= '<div class="add-news-button">' . l(t('Add news item'), 'node/add/ol-news', array(
      'attributes' => array('class' => array('btn btn-xs btn-default add-ol-news')),
    )) . '</div>';
  }

  // Get the promoted news items.
  $news_items = openlucius_news_get_latest_news_items();

  // Initialize news items variable.
  $vars['news_items'] = '';

  // Initialize build.
  $build = array();

  // Loop through the results.
  foreach ($news_items as $news_item) {

    // Initialize item array.
    $item = array(
      'title' => $news_item->title,
    );

    // Do a user load.
    $account = user_load($news_item->uid);

    // Empty it, else a user without image will get image from another user.
    $item['picture'] = '';

    // Get picture.
    $picture = openlucius_core_fetch_user_image($account, '65x65');

    // Check picture.
    if (!empty($picture)) {

      // Set picture.
      $item['picture'] = $picture;
    }

    $item['user_path'] = 'user/' . $account->uid;
    $item['username'] = check_plain($account->name);
    $item['time_ago'] = format_interval(($_SERVER['REQUEST_TIME'] - $news_item->created), 1);
    $item['nid'] = $news_item->nid;

    if (!empty($news_item->body_value)) {
      openlucius_core_render_read_more($news_item->body_value, $item, TRUE);
    }

    $attachment_query = db_select('field_data_field_shared_files', 'f')
      ->fields('f', array('field_shared_files_fid'))
      ->condition('f.field_shared_files_display', 1, '=')
      ->condition('entity_type', 'node', '=')
      ->condition('entity_id', $news_item->nid, '=');

    // Fetch all.
    $attachments = $attachment_query->execute()->fetchAll();

    // Process attachments.
    _openlucius_core_process_recent_stuff_attachments($attachments, $item);

    // Status update like button.
    $item['like'] = flag_create_link(OPENLUCIUS_CORE_SOCIAL_LIKE_NODE_FLAG, $news_item->nid);

    // Get the likers.
    $likers = openlucius_core_social_get_likes('node', $news_item->nid, OPENLUCIUS_CORE_SOCIAL_LIKE_NODE_FLAG);

    $item['like_count'] = '';

    if (!empty($likers)) {
      $item['likes'] = $likers;
      $item['like_count'] = count($likers);

      // Add the like string.
      openlucius_core_social_format_likers_string($likers, $item);
    }

    // Get the comments.
    $comments = openlucius_core_comments_on_node($news_item->nid);

    // Initialize comment count.
    $item['comment_count'] = '';

    // Check if there are comments.
    if (!empty($comments)) {

      // Set the comment count.
      $item['comment_count'] = count($comments);

      // Initialize the comments array.
      $item['comments'] = array();

      // Loop through the comments.
      foreach ($comments as $c => $comment) {

        // Load the user of the comment.
        $comment_user = user_load($comment->uid);

        // The comment user path.
        $item['comments'][$c]['user_path'] = url('user/' . $comment->uid);

        // Check for realname.
        if (module_exists('realname')) {

          // Check if the realname was found.
          if (!empty($comment_user->realname)) {
            $item['comments'][$c]['username'] = check_plain($comment_user->realname);
          }
        }
        else {
          $item['comments'][$c]['username'] = check_plain($comment_user->name);
        }

        // Empty it, else a user without image will get image from another user.
        $item['comments'][$c]['picture'] = '';

        // Get picture.
        $picture = openlucius_core_fetch_user_image($comment_user, 'ol_30x30');

        // Check picture.
        if (!empty($picture)) {

          // Set picture.
          $item['comments'][$c]['picture'] = $picture;
        }

        // Check for the comment body value.
        if (!empty($comment->comment_body_value)) {
          $item['comments'][$c]['comment_body_value'] = $comment->comment_body_value;
        }

        $item['comments'][$c]['like'] = openlucius_core_social_create_like_comment_flag($comment->cid);

        // Check the timestamp.
        if (!empty($comment->changed)) {
          $item['comments'][$c]['timestamp'] = format_interval(($_SERVER['REQUEST_TIME'] - $comment->changed), 1);
        }
      }

      // Get the threshold for hidden.
      $item['comment_count'] = count($item['comments']);
    }

    // Do show the comment form.
    $item['show_comment_form'] = TRUE;

    $item['user_picture'] = '';

    // Get picture.
    $picture = openlucius_core_fetch_user_image($user->uid, 'ol_30x30');

    // Check picture.
    if (!empty($picture)) {

      // Set picture.
      $item['user_picture'] = $picture;
    }

    // Add border if possible.
    openlucius_stream_item_border($item);

    // Load the forms.
    module_load_include('inc', 'openlucius_core', 'includes/forms');

    // Get the form.
    $form = drupal_get_form('openlucius_core_activity_item_reply_form', $item['nid']);

    // Add comment form to reply directly on the status update.
    $item['comment_form'] = drupal_render($form);

    // Return the themed item.
    $vars['recent_stuff_items'] .= theme('openlucius_core_recent_stuff_item', $item);
  }

  // Stuff rows in wrapper.
  $content = theme('openlucius_core_recent_stuff', array('vars' => $vars));

  // Add the content.
  $build['content'] = array('#markup' => $content);

  // Get the news limit.
  $limit = variable_get('openlucius_news_news_limit', 20);

  $build['pager'] = array(
    '#markup' => theme('pager', array(
      'element'  => $GLOBALS['pager_id'],
      'quantity' => $limit,
    )),
  );

  // Build the output.
  $output .= drupal_render($build);

  return $output;
}

/**
 * Function to generate the output for the promoted news items.
 *
 * @return string
 *   Returns a string of output.
 */
function openlucius_news_build_promoted_news_items_display() {

  // Get the promoted news items.
  $promoted_items = openlucius_news_get_promoted_news_items();

  // Initialize promoted news items variable.
  $output = '';

  // Loop through the results.
  foreach ($promoted_items as $vars) {

    // Add the promoted news item link to the output.
    $output .= openlucius_news_build_promoted_news_item($vars);
  }

  // Return the build.
  return $output;
}

/**
 * Function to format a link for promoted news.
 *
 * @param \stdClass $vars
 *   The variables for the promoted news item.
 *
 * @return string
 *   Returns the link as html string.
 */
function openlucius_news_build_promoted_news_item(\stdClass $vars) {

  // Initialize item array.
  $item = array();

  // Set the node id.
  $item['nid'] = $vars->nid;

  // Load the user that promoted the item.
  $account = user_load($vars->uid);

  // Empty it, else a user without image will get image from another user.
  $item['picture'] = '';

  // Get picture.
  $picture = openlucius_core_fetch_user_image($account, '65x65');

  // Check picture.
  if (!empty($picture)) {

    // Set picture.
    $item['picture'] = $picture;
  }

  // Set the user path.
  $item['user_path'] = url('user/' . $vars->uid);

  // Check for realname.
  if (module_exists('realname')) {

    // Check if the realname was found.
    if (!empty($account->realname)) {
      $item['username'] = check_plain($account->realname);
    }
  }
  else {
    $item['username'] = check_plain($account->name);
  }

  // Set the timestamp.
  $item['time_ago'] = format_interval(($_SERVER['REQUEST_TIME'] - $vars->created), 1);

  // Set the title.
  $item['group_title'] = check_plain($vars->title);

  // Get the node body.
  $item['node_body'] = isset($vars->body_value) ? check_markup($vars->body_value) : '';

  // Render read more.
  openlucius_core_render_read_more($item['node_body'], $item);

  $attachment_query = db_select('field_data_field_shared_files', 'f')
    ->fields('f', array('field_shared_files_fid'))
    ->condition('f.field_shared_files_display', 1, '=')
    ->condition('entity_type', 'node', '=')
    ->condition('entity_id', $vars->nid, '=');

  // Fetch all.
  $attachments = $attachment_query->execute()->fetchAll();

  // Process attachments.
  _openlucius_core_process_recent_stuff_attachments($attachments, $item);

  // Don't show the comment form.
  $item['show_comment_form'] = FALSE;

  // Loop through the files.
  foreach ($attachments as $attachment) {

    // Load the file.
    $file = file_load($attachment->field_shared_files_fid);

    // Check if the file exists.
    if (file_exists($file->uri)) {

      // Check if the file is an image.
      if (stristr($file->filemime, 'image/')) {

        // Create images array.
        $images[] = $file;

        // Check images array.
        if (is_array($images)) {

          // Create image from the first image.
          $item['image'] = theme('image_style', array(
            'style_name' => 'ol_100x100',
            'path'       => $images[0]->uri,
          ));
        }
      }
    }
  }

  // Add the ribbon html.
  $item['content_top'] = '<div class="ribbon-wrapper"><div class="ribbon"><span>' . t('Highlight') . '</span></div></div>';

  // Return the output.
  return theme('openlucius_core_recent_stuff_item', $item);
}
