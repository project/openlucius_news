<?php
/**
 * @file
 * This file contains all forms for the news.
 */

/**
 * Implements hook_form_comment_node_ol_news_form_alter().
 */
function openlucius_news_form_comment_node_ol_news_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['field_wrapper'])) {
    unset($form['field_wrapper']);
  }
}

/**
 * Implements hook_form_ol_news_node_form_alter().
 */
function openlucius_news_form_ol_news_node_form_alter(&$form, &$form_state, $form_id) {

  // Add extra form submit for redirecting.
  $form['actions']['submit']['#submit'][] = 'openlucius_news_custom_save';

  if (user_access('promote news')) {
    // Allow users to promote content.
    $form['options']['#access'] = TRUE;
    $form['options']['promote']['#access'] = TRUE;
    $form['options']['sticky']['#access'] = FALSE;
  }

  // Check the node form.
  if ($form['#node'] && $form['#node']->type . '_node_form' === $form_id) {
    if ($form['#node']->nid) {
      $form['options']['status']['#access'] = FALSE;
      $form['actions']['preview_changes']['#access'] = FALSE;
    }
  }
}

/**
 * Implements hook_node_submit().
 */
function openlucius_news_custom_save($form, &$form_state) {
  if ($form_state['values']['type'] == 'ol_news') {
    drupal_goto('recent-stuff', array(
      'fragment' => 'news',
    ));
  }
}

/**
 * Form constructor for the news configuration.
 */
function openlucius_news_configuration_form() {

  $form = array();

  $form['openlucius_news_news_limit'] = array(
    '#title'         => t('News limit'),
    '#description'   => t('The amount of news items to show on each page'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('openlucius_news_news_limit', 20),
  );

  $form['openlucius_news_show_news_clients'] = array(
    '#title'         => t('Show news to clients'),
    '#description'   => t('Whether the news should be visible for clients.'),
    '#type'          => 'checkbox',
    '#default_value' => variable_get('openlucius_news_show_news_clients', 1),
  );

  return system_settings_form($form);
}

/**
 * Form constructor for the status updates.
 *
 * @ingroup forms
 */
function openlucius_news_reply_form($form, &$form_state, $nid) {

  // Initialize the form.
  $form = array(
    '#attributes' => array(
      'class' => array(
        'update-reply-form',
        'update-reply-form-' . $nid,
      ),
    ),
  );

  // The hidden node id.
  $form['openlucius_news_comment_nid'] = array(
    '#type'  => 'hidden',
    '#value' => $nid,
  );

  // The body form element.
  $form['openlucius_news_comment_body'] = array(
    '#type'       => 'textarea',
    '#required'   => TRUE,
    '#rows'       => 2,
    '#attributes' => array(
      'placeholder' => t('Post a reply'),
    ),
  );

  // The ajax form submit.
  $form['submit'] = array(
    '#type'       => 'submit',
    '#value'      => t('Post reply') . ' ',
    '#attributes' => array(
      'class' => array(
        'reply-on-status-update',
        'btn-success',
        'btn-xs',
      ),
    ),
    '#ajax'       => array(
      'callback' => 'openlucius_news_reply_ajax_callback',
      'wrapper'  => 'update-container',
      'effect'   => 'fade',
      'progress' => array(
        'message' => ' ',
        'type'    => 'throbber',
      ),
    ),
  );

  // Return the form.
  return $form;
}

/**
 * Ajax form submit callback function.
 */
function openlucius_news_reply_ajax_callback($form, &$form_state) {

  // The logged in user.
  global $user;

  // The form state values.
  $values = $form_state['values'];
  $nid = $values['openlucius_news_comment_nid'];
  $body = $values['openlucius_news_comment_body'];

  // Check for the comment body.
  if (!empty($body)) {

    // Create comment subject, @see comment_submit().
    $comment_text = check_markup($body);
    $subject = truncate_utf8(trim(decode_entities(strip_tags($comment_text))), 29, TRUE);

    // Create new comment object.
    $comment = (object) array(
      'nid'          => $nid,
      'cid'          => 0,
      'pid'          => 0,
      'uid'          => $user->uid,
      'mail'         => '',
      'is_anonymous' => 0,
      'homepage'     => '',
      'status'       => COMMENT_PUBLISHED,
      'subject'      => $subject,
      'language'     => LANGUAGE_NONE,
      'comment_body' => array(
        LANGUAGE_NONE => array(
          0 => array(
            'value'  => $body,
            'format' => 'plain_text',
          ),
        ),
      ),
    );

    // Store values for below.
    $cid = $comment->cid;
    $user = user_load($user->uid);
    $user_picture_fid = isset($user->picture->fid) ? $user->picture->fid : 0;

    // Submit and save the comment.
    comment_submit($comment);
    comment_save($comment);

    // Build the comment html.
    $comment = openlucius_core_social_format_comment(array(
      'uid'     => $user->uid,
      'cid'     => $cid,
      'nid'     => $nid,
      'name'    => $user->name,
      'body'    => $body,
      'picture' => $user_picture_fid,
    ));

    // Set the target.
    $target = '.update-reply-form-' . $nid;

    // Build the commands array.
    $commands[] = ajax_command_before($target, $comment);
  }

  // Reset the form.
  $commands[] = ajax_command_invoke(NULL, 'statusUpdateCommentAdded');

  // Return.
  return array(
    '#type'     => 'ajax',
    '#commands' => $commands,
  );
}
