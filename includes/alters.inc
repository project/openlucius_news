<?php
/**
 * @file
 * This file contains the alters for the openlucius notifications module.
 */

/**
 * Implements openlucius_core_frontpage_tabs_alter().
 */
function openlucius_news_openlucius_core_frontpage_tabs_alter(&$items) {

  // Check if the either news items are enabled or if the user is not a client.
  if (variable_get('openlucius_news_show_news_clients', 1) == 1 || !openlucius_core_user_is_client()) {

    // Get the count of unread items.
    $unread_count = openlucius_core_fetch_unread_count('ol_news');

    // Build item.
    $item = array(
      'list-class' => '',
      'target'     => 'news',
      'title'      => t('News'),
      'link-class' => 'news',
      'weight'     => 2,
    );

    if ($unread_count > 0) {

      // Fetch unread.
      $unread = openlucius_core_fetch_unread_count('ol_news', TRUE);

      // Add unread.
      $item['unread'] = $unread;
      $item['mark_read'] = 'ol_news';
    }

    // Add item to front-page tabs for news.
    $items[] = $item;
  }
}

/**
 * Implements hook_openlucius_core_content_types_with_signature_alter().
 */
function openlucius_news_openlucius_core_content_types_with_signature_alter(&$content_types) {
  $content_types[] = 'ol_news';
}

/**
 * Implements hook_openlucius_core_content_types_with_like_button_alter().
 */
function openlucius_news_openlucius_core_content_types_with_like_button_alter(&$content_types) {
  $content_types[] = 'ol_news';
}

/**
 * Implements hook_openlucius_core_content_types_alter().
 */
function openlucius_news_openlucius_core_content_types_alter(&$content_types) {
  $content_types[] = 'ol_news';
}

/**
 * Implements hook_openlucius_core_node_add_access_alter().
 */
function openlucius_news_openlucius_core_node_add_access_alter(&$exclude) {
  $exclude[] = 'ol_news';
}

/**
 * Implements hook_openlucius_core_fetch_notification_users_alter().
 */
function openlucius_news_openlucius_core_fetch_notification_users_alter(&$all_users) {

  // Get all users you are in groups with.
  $all_users = openlucius_core_fetch_associated_users();
}

/**
 * Implements hook_openlucius_core_config_places_alter().
 */
function openlucius_news_openlucius_core_config_places_alter(&$places) {
  $places[] = 'admin/config/openlucius/news';
}

/**
 * Implements hook_node_access().
 */
function openlucius_news_node_access($node, $op, $account) {

  // Check if node type news and user is client.
  if ($node->type == 'ol_news' && (variable_get('openlucius_news_show_news_clients', 1) != 1) && openlucius_core_user_is_client()) {
    return NODE_ACCESS_DENY;
  }
}

/**
 * Implements hook_form_alter().
 */
function openlucius_news_form_alter(&$form, &$form_state, $form_id) {
  if (stripos($form_id, 'node_form') && $form['type']['#value'] == 'ol_news' && $form['nid']['#value'] == NULL) {
    $form['title']['#default_value'] = strip_tags(t('New !type', array('!type' => node_type_get_name($form['type']['#value'])), array('context' => 'openlucius_news')));
  }
}
