
-- SUMMARY --
This module contains the news module for openlucius.
After enabling the module you'll have a new content type "news"
The front page will have a separate tab containing the news items and promoted
items.

-- REQUIREMENTS --
Requires the Openlucius distribution.

-- INSTALLATION --
Install this module as you would install any other module.
