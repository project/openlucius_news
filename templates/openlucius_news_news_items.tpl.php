<?php
/**
 * @file
 * Template file containing the wrapper for news items.
 */
?>
<div class="timeline-wrapper news-items">
  <ul class="timeline news-items">
    <?php print $vars['news_items']; ?>
  </ul>
</div>
