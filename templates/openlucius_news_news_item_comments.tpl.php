<?php
/**
 * @file
 * Displays a wrapper for status-update comment items.
 */
?>
<div class="comment-items-wrapper">
  <?php print (!empty($vars['news_comment']) ? $vars['news_comment'] : ''); ?>
  <?php print (!empty($vars['comment_form']) ? $vars['comment_form'] : ''); ?>
</div>
