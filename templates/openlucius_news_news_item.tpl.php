<?php
/**
 * @file
 * Template for a single news item.
 */
?>
<li class="timeline news-item" data-id="<?php print (!empty($vars['nid']) ? $vars['nid'] : ''); ?>">
  <div class="activity_item news_activity">
    <div class="timeline-badge views-field-picture">
      <span class="glyphicon glyphicon-user"></span>
      <?php if (isset($vars['picture'])): ?>
        <?php print $vars['picture']; ?>
      <?php endif; ?>
    </div>
    <div class="timeline-panel">
      <div class="timeline-heading">
        <h5 class="timeline-title">
          <strong><?php print $vars['username']; ?></strong>
          <?php print $vars['between_text']; ?>
          <strong><?php print $vars['title']; ?></strong>
        </h5>
        <p>
          <small class="text-muted time-ago">
            <i class="glyphicon glyphicon-transfer"></i>
            <?php print $vars['time_ago']; ?>
          </small>
        </p>
        <?php if (isset($vars['first'])) : ?>
          <blockquote class="first"><?php print $vars['first']; ?></blockquote>
        <?php endif; ?>

        <?php if (isset($vars['last'])) : ?>
          <blockquote class="last"><?php print $vars['last']; ?></blockquote>
        <?php endif; ?>

        <?php if (isset($vars['full'])) : ?>
          <blockquote><?php print $vars['full']; ?></blockquote>
        <?php endif; ?>

        <?php if (isset($vars['more_button'])) : ?>
          <?php print ($vars['more_button']); ?>
        <?php endif; ?>

        <?php if (isset($vars['less_button'])) : ?>
          <?php print ($vars['less_button']); ?>
        <?php endif; ?>
      </div>
      <?php if (!empty($vars['gallery'])): ?>
        <?php print $vars['gallery']; ?>
      <?php endif; ?>

      <?php if (!empty($vars['files'])): ?>
        <?php print $vars['files']; ?>
      <?php endif; ?>
      <div class="status-update-actions">
        <?php if (!empty($vars['like_flag'])): ?>
          <div class="status-update-like">
            <?php print $vars['like_flag']; ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($vars['likes'])): ?>
          <br/>
          <?php print $vars['likes']; ?>
        <?php endif; ?>
      </div>
      <?php if (!empty($vars['news_comments'])): ?>
        <div class="comment-container comment-container-<?php print (!empty($vars['nid']) ? $vars['nid'] : ''); ?>">
          <?php print $vars['news_comments']; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</li>
